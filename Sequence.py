"""A sequence of a benchmark represented using TUM RGB-D standards."""
import os
import sys
import glob
import subprocess
import argparse
import warnings
import numpy as np
from pathlib import Path
import Trajectory
import associate as tum
import evaluate_ate as ate
# Force matplotlib to not use any Xwindows backend.
import matplotlib
import ProbabilityScale
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('grayscale')

np.set_printoptions(precision=4, suppress=True)


def align_trjs(gtf, trf, max_diff=0.02, offset=0.0):
    """Align two trajectories according to their matching poses."""
    # Search timestamps matches between both trajectories
    # Use a copy of the list of timestamps since some of them will be removed
    matches = tum.associate(list(gtf.t), list(trf.t), offset, max_diff)
    if len(matches) < 2:
        sys.exit("[ERROR] Couldn't match stamps between GT and trajectory!")

    import copy
    gt = copy.deepcopy(gtf)
    tr = copy.deepcopy(trf)
    gt.filter_ts([a for a, b in matches])
    tr.filter_ts([b for a, b in matches])

    # Align trajectories wrt Sim3 and the 1st matched quaternion difference
    s, R, T = tr.get_Sim3(gt)
    tr.apply_Sim3(s, R, T, gt.q[0] * tr.q[0].conjugate())
    trf.apply_Sim3(s, R, T)

    return gt, tr
    # return (first_stamps, first_xyz, first_xyz_full, first_matches, first_q),
    #     (second_stamps, second_xyz_aligned, second_xyz_full_aligned,
    #      second_matches, second_q), (trans_error, rot, trans, scale)


class Sequence(object):
    """A sequence of frames labelled with a timestamp."""

    PATH_KN = Path('/data/dev/slam')
    PATH_ORB = Path('/data/dev/ORB_SLAM2')
    PATH_PTAM = Path('/data/dev/gptam')

    def __init__(self, seq):
        """
        Build a new sequence from a dict.

        :param seq: the sequence attributes
        :type seq: dict
        """
        self._path = Path(seq.get('path'))
        if not self._path.is_dir():
            raise RuntimeError('{} does not exists'.format(self._path))
        self.kf1 = seq.get('kf1', None)
        self.kf2 = seq.get('kf2', None)
        self.results = {}
        self._settings = None
        self.load()
        self._gt = Trajectory.Trajectory(self.gt_path, 'gt')

    def load(self):
        """Load all (timestamp, filename) associations for this sequence."""
        if hasattr(self, 'frames'):
            return

        fn = self._path / 'rgb.txt'
        with open(fn, 'r') as fin:
            data = fin.read()
            lines = data.replace(",", " ").replace("\t", " ").split("\n")
            values = [[v.strip() for v in line.split(" ") if v.strip() != ""]
                      for line in lines if len(line) > 0 and line[0] != "#"]
            values = [(float(l[0]), l[1]) for l in values if len(l) > 1]
        self.frames = dict(values)

    def search_first_frame(self, trj):
        """
        Search the first keyframe of the trajectory trj.

        :param trj: the trajectory
        :type trj: Trajectory
        """
        ts = list(self.frames)
        ts.sort()
        for idx, t in enumerate(ts):
            if trj.t[0] == t:
                return idx
        return len(ts)

    @property
    def path(self):
        """Return the path of the sequence."""
        return str(self._path)

    @property
    def duration(self):
        """Return the time duration of the sequence."""
        ts = list(self.frames)
        return float(ts[len(ts)-1]) - float(ts[0])

    @property
    def length(self):
        """Return the total number of frames associated to the sequence."""
        return len(self.frames)

    @property
    def gt_tr(self):
        """Return the trajectory of the ground truth of this sequence."""
        return self._gt

    @property
    def gt_path(self):
        """Return the path of the ground truth file for this sequence."""
        return '{}.gt.txt'.format(self.path)

    def tr_glob(self, slam):
        """Return the list of trajectories for a specific slam approach."""
        path = self.slam_path(slam)
        return glob.glob('{}/*.tr.txt'.format(path))

    def slam_path(self, slam):
        """Get the path of the results for a SLAM method."""
        return self._path / slam

    def res_path(self, slam):
        """Get the path of the results file for a SLAM method."""
        return self.slam_path(slam) / 'results.csv'

    def truncate(self, slam):
        """Truncate the results for a SLAM method."""
        try:
            with open(self.res_path(slam), 'w') as f:
                f.write('tr, k, e, c, p, q\n')
        except OSError:
            pass

    def append_result(self, slam, d):
        """
        Add new entry to the results table.

        :param slam: the SLAM method
        :param d: the data array to append
        """
        with open(self.res_path(slam), 'a') as f:
            fmt = '{}, {:d}, {:d}, {:.4f}, {:.6f}, {:.6f}\n'
            f.write(fmt.format(d[0], d[1], d[2], d[3], d[4], d[5]))

    def load_res(self, slam):
        """
        Load results for a SLAM method.

        :param slam: the SLAM method
        """
        if slam in self.results:
            return self.results[slam]

        # Load data from file using numpy
        fn = self.res_path(slam)

        if not fn.is_file():
            raise OSError('File {} does not exists'.format(fn))

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            dtype = ('|S13', float, float, float, float, float)
            data = np.genfromtxt(fn, delimiter=',', dtype=dtype, skip_header=1)

        if data.size is 0:
            raise RuntimeError('0 samples for {}'.format(self.path))

        # Handle special case: just one sample
        if data.ndim is 0:
            data = [data.tolist()]

        self.results[slam] = data
        return self.results[slam]

    def is_outlier(self, slam):
        """Check if the sequence should be labelled as outlier."""
        rst = self.load_res(slam)
        avg = np.mean([r[3] for r in rst])
        return rst.shape[0] < 3 or avg < 0.3

    def get_inliers(self, slam, N=5):
        """
        Get the N values around the median of the slam method.

        :param slam: a string with the ID of the SLAM system
        :param N: the the number of items to select around the median
        :return: values of the N inliers
        """
        # Load results for slam
        rs = self.load_res(slam)
        data = np.array([r[4] for r in rs])
        # Select the indices of the N values closest to the median
        m = np.median(data)
        d = np.abs(data - m)
        inliers = d.argsort()[:N]
        return data[inliers]

    @property
    def percentile(self, slam, val):
        """
        Compute the percentile of a value in the results.

        :param slam: the SLAM system to be inspected
        :param val: the value for searching its percentile
        """
        # Load results for slam
        rs = self.load_res(slam)
        data = np.array([r[4] for r in rs])
        # Find the indices where the value laid
        data = np.sort(data)
        pos = np.searchsorted(data, val)
        # Compute distance to the start of the index
        d = (val - data[pos-1]) / (data[pos] - data[pos-1])
        per = 100.0 * (pos + d) / len(data)
        return per

    @property
    def benchmark(self):
        """Get the benchmark of this sequence."""
        n = len(self._path.parts) - 2
        return self._path.parts[n]

    @property
    def name(self):
        """Retrieve the name of this sequence."""
        n = len(self._path.parts) - 1
        return self._path.parts[n]

    @property
    def settings(self):
        """Smart settings according to the sequence's path."""
        if self._settings is not None:
            return self._settings

        path_str = self.path
        if 'tum' in path_str:
            if 'fr1' in path_str:
                self._settings = 'tum1'
            elif 'fr2' in path_str:
                self._settings = 'tum2'
            elif 'fr3' in path_str:
                self._settings = 'tum3'
        elif 'kitti' in path_str:
            if '00' in path_str or '01' in path_str or '02' in path_str:
                self._settings = 'kitti00-02'
            elif '03' in path_str:
                self._settings = 'kitti03'
            else:
                self._settings = 'kitti04-12'
        elif 'icl' in path_str:
            self._settings = 'icl'
        elif 'euroc' in path_str:
            self._settings = 'euroc'
        elif 'agz' in path_str:
            self._settings = 'agz'
        else:
            raise RuntimeError('Unknown settings for {}'.format(self.path))

        return self._settings

    @property
    def settings_path(self):
        """Get the complete path of the settings file."""
        return '{}/data/{}.yml'.format(self.PATH_KN, self.settings)

    def run_kn_slam(self):
        """Execute the external command to launch a KN-SLAM experiment."""
        subprocess.call([
            '{}/release/mono_tum'.format(self.PATH_KN),
            '{}/data/orb-voc.proto'.format(self.PATH_KN),
            '{}/data/{}.yml'.format(self.PATH_KN, self.settings),
            self.path,
            'gui'
        ])

    def run_orb_slam(self):
        """Execute the external command to launch a ORB-SLAM experiment."""
        subprocess.call([
            '{}/build/mono_tum'.format(self.PATH_ORB),
            '{}/Vocabulary/ORBvoc.txt'.format(self.PATH_ORB),
            '{}/data/{}.yml'.format(self.PATH_KN, self.settings),
            self.path
        ])

    def run_ptam(self):
        """Execute the external command to launch a PTAM experiment."""
        if self.kf1 is None or self.kf2 is None:
            raise RuntimeError('PTAM cannot run for {}'.format(self.path))

        subprocess.call([
            '{}/build/ptam'.format(self.PATH_PTAM),
            '{}/data/{}.cfg'.format(self.PATH_PTAM, self.settings),
            self.path,
            str(self.kf1),
            str(self.kf2)
        ])

    def run(self, slam):
        """Benchmark the sequence with the SLAM system linked to the label."""
        runners = {
            'kn': self.run_kn_slam,
            'orb': self.run_orb_slam,
            'ptam': self.run_ptam
        }
        if slam not in runners:
            print('[ERROR] No runner for {}. Using kn-slam instead'.format(slam))
            exit(-1)
        return runners[slam]()

    def plot(self, slams):
        """
        Plot different and interesting data about this trajectory.

        :param slams: a list of SLAM approaches to compare
        """
        plt_path = self._path / 'plots'
        os.makedirs(plt_path, exist_ok=True)
        seq_name = self.name.replace('_', '-')

        # Plot the box diagram to measure dispersion
        if len(slams) > 1:
            fn = plt_path / '{}-box.pdf'.format(seq_name)
            self.plot_box(slams, str(fn))

        # Plot trajectories in separated files
        for slam in slams:
            fn = plt_path / '{}-{}-tr.pdf'.format(seq_name, slam)
            self.plot_trj(slam, fn)

        # Plot for each SLAM evaluated in this sequence
        for slam in slams:
            self.plot_errors(slam)

    def plot_box(self, slams, plot_fn, seqs=[]):
        """Draw a boxplot diagram to compare the distribution of the errors."""
        data = []
        labels = []
        if len(seqs) is 0:
            seqs.append(self)
        for seq in seqs:
            labels.append(seq.name)
            for slam in slams:
                try:
                    data.append(seq.get_inliers(slam))
                except Exception:
                    data.append(0.0)
                    pass

        # Draw the box plot with the result of all iterations
        _, ax = plt.subplots(1)
        ax.boxplot(data)
        ax.set_xticks(np.arange(1.5, 2*len(labels), 2.0))
        ax.set_xticklabels([s.replace('_', '\_') for s in labels])

        # Tweak spacing to prevent clipping of tick-labels
        # plt.subplots_adjust(bottom=0.3)
        plt.tight_layout()
        plt.savefig(plot_fn)
        plt.close()

    def plot_bar(self, slams, plot_fn, seqs=[], key='kn'):
        """
        Plot a bar graph with the ATE of several sequences.

        :param slams: the SLAM methods to compare for each sequence
        :param plot_fn: the filename to save the plot
        :param seqs: the list of sequences to compare
        :param key: the slam system from which sort the sequences
        """
        if key not in slams:
            raise RuntimeError('key {} must be in {}'.format(key, slams))

        if len(seqs) is 0:
            seqs.append(self)

        # Retrieve the median of the ATE for all (sequence, slams)
        # Data will hold tuples for sequences in rows and ATEs in columns
        rows = []
        for seq in seqs:
            noutliers = 0
            row = ['{}/{}'.format(seq.benchmark, seq.name)]
            for slam in slams:
                try:
                    res = seq.load_res(slam)
                    if seq.is_outlier(slam):
                        raise RuntimeError('oulier')
                    y = [r[4] for r in res]
                    row.append(np.median(y))
                except Exception:
                    noutliers += 1
                    row.append(float('nan'))
            if noutliers < len(slams):
                rows.append(row)

        # Create a structured array
        dtype = [('seq', 'U25')]
        for slam in slams:
            dtype.append((slam, 'f4'))
        data = np.array([tuple(r) for r in rows], dtype=dtype)

        # Sort the dict of sequences in descending order
        data[::-1].sort(order=key)

        # Maximum percentual difference allowed in comparisons
        max_x = 50.0
        pad_x = 3.0

        # Dynamically change font settings just for this plot
        import matplotlib as mpl
        mpl.rcParams['font.size'] = 10

        # Create two separated plots with a different aspect ratio
        fig_width = 2 + 0.23 * len(seqs)
        plt.figure(figsize=(11.7, fig_width))
        gs = matplotlib.gridspec.GridSpec(1, 2, width_ratios=[2, 5])
        gs.update(wspace=0.7)
        ax0 = plt.subplot(gs[1])
        # points = np.array([.001, .002, .005, .01, .05, .1, .5, 1])
        # ax0.set_xscale('probability', points=points, lower_bound=.0000001)
        ax0.set_xscale('log', nonposx='mask', basex=10)
        ax0.set_title('Absolute Trajectory Error\n(m in logarithmic scale)')
        ax1 = plt.subplot(gs[0])
        ax1.set_xlim(0, max_x)
        ax1.set_title('Difference\n(\% w.r.t. the worst case)')
        labels = data['seq']
        ticks = np.arange(len(labels))
        all_data = []
        styles = {}

        # Plot ATE in horizontal bars
        for slam in slams:
            styles[slam] = Trajectory.get_style(slam)
            x = data[slam]
            # Plot the markers
            styles[slam]['linestyle'] = 'none'
            ax0.plot(x, ticks, **styles[slam])
            # Plot the horizontal lines
            ax0.hlines(ticks, .001, x, linewidth=1.0)
            all_data = np.concatenate((all_data, x))

        # Plot the difference using the color of the winner
        if len(slams) == 2:
            # Plot line x=0
            ax1.plot([0, 0], [0.0, ticks[len(ticks)-1]], '--', linewidth=1.0)
            diff = 100.0 * (data[slams[0]] - data[slams[1]]) / \
                np.maximum(data[slams[0]], data[slams[1]])
            for i, d in enumerate(diff):
                if np.isnan(d):
                    if np.isnan(data[slams[0]][i]):
                        style = styles[slams[1]]
                        label = Trajectory.get_label(slams[0])
                    else:
                        style = styles[slams[0]]
                        label = Trajectory.get_label(slams[1])
                    xy = (pad_x, ticks[i] - 0.25)
                    ax1.annotate(label.replace('-SLAM', ' - N/A'), xy=xy)
                    ax1.plot(0.0, ticks[i], **style)
                else:
                    x = min(abs(d), max_x - pad_x)
                    s = slams[0] if d < 0 else slams[1]
                    # Plot hline using the color and marker of the best slam
                    style = styles[s]
                    ax1.hlines(ticks[i], 0.0, x, linewidth=1, color=style['color'])
                    ax1.plot(x, ticks[i], **style)
                    # Plot the annotation with the percentual difference
                    xy = (x + pad_x, ticks[i] - 0.25)
                    ax1.annotate('{:2.2f}\%'.format(abs(d)), xy=xy)

        # Place sequence names as labels
        ax0.set_yticks(ticks)
        ax0.set_yticklabels([s.replace('_', '\_') for s in labels], ha='left')
        x = all_data[~np.isnan(all_data)]
        lims = (0.7 * np.min(x), 1.3 * np.max(x))
        ax0.set_xlim(lims)
        pad = max([len(l) for l in labels])
        ax0.get_yaxis().set_tick_params(pad=6 * pad, left=False)
        # Remove y axis for the difference plot
        ax1.set_yticks([])

        # Plot legened and hide axis
        ax0.legend(loc='best', fontsize=12)
        for ax in [ax0, ax1]:
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_visible(False)

        # Save the figure
        plt.savefig(plot_fn)
        plt.close()

    def plot_trj(self, slam, plot_fn):
        """
        Plot a comparison of a trajectory wrt the ground truth.

        :param slam: the slam approach to plot
        :param plot_fn: filename of the plot
        """
        _, ax = plt.subplots(1)
        # Align the median-ATE trajectory wrt GT
        h = self.ate(slam)[0].decode('utf-8')
        tr_fn = self.slam_path(slam) / '{}.tr.txt'.format(h)
        trf = Trajectory.Trajectory(tr_fn, slam)
        self.align_gt(trf)  # trf poses will be aligned inside the procedure

        # Plot both trajectories on a single figure alongside the GT
        self.gt_tr.style['marker'] = None
        trf.style['marker'] = None
        self.gt_tr.plot(ax)
        trf.plot(ax)
        self.gt_tr.adjust_ax(ax)

        # Save figure in disk
        plt.tight_layout()
        plt.savefig(plot_fn)
        plt.close()

    def plot_errors(self, slam):
        """Plot errors for every pair (trajectory, SLAM)."""
        path = self.slam_path(slam)
        for f in os.listdir(path):
            # Discard possible non-trajectory files
            if not f.endswith('.tr.txt'):
                continue

            print('Processing trajectory {}'.format(f))
            root = str(path / f).replace('.tr.txt', '')

            # Align the trajectory wrt its ground truth
            trf = Trajectory.Trajectory(str(path / f), slam)
            gt, tr = self.align_gt(trf)

            # Plot the trajectory wrt GT
            _, ax = plt.subplots()
            self.gt_tr.plot(ax)
            trf.plot(ax)
            trf.adjust_ax(ax)
            plt.savefig('{}_tr.pdf'.format(root))
            plt.close()

            # Plot the accumulated average rotational error vs time
            _, ax = plt.subplots()
            tr.plot_qa(gt, ax)
            plt.savefig('{}.qa.pdf'.format(root))
            plt.close()

            # Plot the translational error(t) for both trajectories
            # fig, ax = plt.subplots()
            # to.plot_p(gto, ax)
            # tf.plot_p(gtf, ax)
            # ax.legend(loc='best', fontsize='12', fancybox=True, framealpha=0.5)
            # fig.tight_layout()
            # plt.savefig('{}.p.pdf'.format(root))
            # plt.close()

            # Plot the rotational error vs time
            fig, ax = plt.subplots()
            tr.plot_q(gt, ax)
            plt.savefig('{}.q.pdf'.format(root))
            plt.close()

            # Plot both errors vs time
            # fig, ax = plt.subplots(1)
            # tr.plot_p(gt, ax)
            # tr.plot_q(gt, ax.twinx())
            # ax.legend(loc='best', fontsize='12', fancybox=True, framealpha=0.5)
            # plt.tight_layout()
            # plt.savefig('{}_r.pdf'.format(root))
            # plt.close()

            # Plot histogram of edge weights
            # hf_fn = '{}.kn.hst.txt'.format(root)
            # ho_fn = '{}.orb.hst.txt'.format(root)
            # edges.plot_cmp(hf_fn, ho_fn, '{}.h.pdf'.format(plt_path))

            # Plot the map points
            # fig, ax = plt.subplots(1)
            # tf.plot_map(ax, '{}.kn.map.ply'.format(root))
            # plt.savefig('{}.map.pdf'.format(root))
            # plt.close()

    def cover(self, slam, col_ptt=3):
        """
        Compute the mean of the percentage of tracked trajectory.

        :param slam : the SLAM method
        :param col_ptt: the column with the percentage of trajectory
        :return: (number of executions, mean of reconstructed trajectory)
        """
        data = self.load_res(slam)
        ptt = np.array([r[col_ptt] for r in data])
        avg = np.mean(ptt, axis=0)
        return [str(ptt.shape[0]), '{:.4f}'.format(avg)]

    def ate(self, slam, col=4):
        """
        Retrieve the row with the median in a specific column.

        :param fb: the filename with a matrix of data in CSV
        :param col: the column holding ATE value
        :return: the row of the matrix with the median value
        """
        data = self.load_res(slam)

        # Convert to matrix and remove the first column (date)
        mat = np.array([[r[i] for i in range(1, 6)] for r in data])

        # Select those entries with more than 50% of trajectory
        # inliers = np.where(data[:, 2] > 50.0)[0]

        # Compute the median in even and odd cases
        N = mat.shape[0]
        e = mat[:, col-1]
        if N % 2 == 0:
            indices = np.argsort(e)
            m = len(e) // 2
            i = indices[m-1]
            j = indices[m]
            return data[i][0], N, np.mean(mat[[i, j], :], axis=0)
        else:
            i = np.argsort(e)[len(e) // 2]
            return data[i][0], N, mat[i]

    def align_gt(self, tr, max_diff=0.02, offset=0.0):
        """Align a trajectory wrt the ground truth for this sequence."""
        return align_trjs(self.gt_tr, tr, max_diff, offset)
