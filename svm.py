"""
Use SVM to evaluate the influence of sequence charactristics in the results.

Test other machine learning algorithms for the sake of simple evaluation.
"""

import sys
import numpy as np
from sklearn.svm import SVC, SVR
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.feature_selection import RFE, SelectKBest, chi2
from sklearn.metrics import accuracy_score
import matplotlib
import matplotlib.pyplot as plt
from cycler import cycler


if len(sys.argv) < 2:
    print('Use svm.py <csv_file>')
    exit(-1)

np.set_printoptions(precision=4, suppress=True)

# Load data from CSV file
# Load data from CSV file
names = ['fast', 'shake', 'close', 'rotation', 'translation',
         'dynamic', 'loops', 'hftex', 'dark']

# Parse information from the feature-weights table
data = np.genfromtxt(sys.argv[1], dtype=np.str, delimiter=';')
data = np.char.replace(data, ',', '.')
data = np.char.replace(data, '\'', '')
data = np.char.replace(data, 'b', '').astype(np.float64)


def get_XY(data, start=0, end=38):
    """Retrieve a data partition of a dataset."""
    d = data[start:end]
    X = d[:, 1:10]
    ate = d[:, 10]
    Y = np.empty((ate.shape), dtype=object)
    Y[ate > 0.0] = 'KN'
    Y[ate < 0.0] = 'ORB'
    return X, Y


def test_chi2():
    """Use univariate feature selection using chi-squared function."""
    kbest = SelectKBest(score_func=chi2, k=1)
    X, Y = get_XY(data)
    fit = kbest.fit(X, Y)
    scores = np.nan_to_num(fit.scores_)
    indices = np.argsort(scores)[::-1]
    # print('Weights [Chi2]: {}'.format(scores))
    # print('Ranking [Chi2]: {}'.format([names[i] for i in indices]))


def test_tree():
    """Use ExtraTreesClassifier to rank the features."""
    model = ExtraTreesClassifier()
    model.fit(X, Y)
    indices = np.argsort(model.feature_importances_)[::-1]
    # print('Weights [Tree]: {}'.format(model.feature_importances_))
    # print('Ranking [Tree]: {}'.format([names[i] for i in indices]))


def test_rfe(model=None):
    """Test Recursive Feature Elimination (RFE) with the linear classifier."""
    if model is None:
        model = SVC(kernel='linear', probability=True)
    rfe = RFE(model, 1, step=1)
    fit = rfe.fit(X, Y)
    A = rfe.predict(X)
    accuracy = (Y == A).mean()
    # features = [names[i] for i, x in enumerate(fit.support_) if x]
    # print("Best [RFE]: {}".format(features))
    ranking = [0] * len(names)
    for i, x in enumerate(fit.ranking_):
        ranking[x-1] = names[i]
    # print('Accuracy [RFE]: {}'.format(accuracy))
    # print('Ranking  [RFE]: {}'.format(ranking))


def get_header():
    """Return the titles of the fields for the result vector."""
    return names + ['ind', 'C', 'margin', 'acc']


def test_svc(c):
    """Test the SVM classifier (SVC) with a specific penalty parameter C."""
    # linearSVC is better suited for big number of sequences
    # lsvc = LinearSVC(C=1.0, penalty="l1", dual=False)
    # Train a linear SVM classifier
    clf = SVC(kernel='linear', C=c, gamma=1/9, probability=True)
    X, Y = get_XY(data)
    clf.fit(X, Y)
    # Compare prediction with observations
    # X, Y = get_XY(data, 30, 45)
    A = clf.predict(X)
    # print(clf.decision_function(X))
    accuracy = accuracy_score(Y, A, normalize=True)
    margin = 1 / np.sqrt(np.sum(clf.coef_ ** 2))
    return clf.coef_[0].tolist() + [clf.intercept_[0], c, margin, accuracy]
    # indices = np.argsort(scores)[::-1]
    # print('Ranking [SVMC]: {}'.format([names[i] for i in indices]))


def test_svr(c):
    """Test the SVM regression (SVR) with a specific penalty parameter C."""
    lsvc = SVR(kernel='linear', C=c, epsilon=0.2)
    lsvc.fit(X, ate)
    # Compare prediction with observations
    E = ate - lsvc.predict(X)
    acc = np.abs(E.mean())
    return np.abs(lsvc.coef_[0]).tolist() + [lsvc.intercept_[0], c, acc]


def test_logreg(c):
    """Test Probabilistic Logistic Regression method."""
    logr = LogisticRegression(C=c, penalty="l1", dual=False)
    X, Y = get_XY(data)
    logr.fit(X, Y)
    # scores = np.abs(logr.coef_[0])
    # indices = np.argsort(scores)[::-1]
    # print('Weights [LogR]: {}'.format(logr.coef_[0]))
    # print('Ranking [LogR]: {}'.format([names[i] for i in indices]))
    acc = accuracy_score(Y, logr.predict(X), normalize=True)
    margin = 1 / np.sqrt(np.sum(logr.coef_ ** 2))
    return logr.coef_[0].tolist() + [logr.intercept_[0], c, margin, acc]


def test_mlp(c=1e-5):
    """Train a MLP with an identity activation function to preserve linearity."""
    mlp = MLPClassifier(solver='lbfgs', alpha=c,
                        activation='identity', hidden_layer_sizes=(1),
                        random_state=1, learning_rate='adaptive')
    X, Y = get_XY(data)
    mlp.fit(X, Y)
    scores = mlp.coefs_[0].flatten()
    # print('Weights [MLPC]: {}'.format(scores))
    # indices = np.argsort(scores)[::-1]
    # print('Ranking [MLPC]: {}'.format([names[i] for i in indices]))
    acc = accuracy_score(Y, mlp.predict(X), normalize=True)
    return scores.tolist() + [mlp.intercepts_[0][0], c, mlp.loss_, acc]


def print_best(results):
    """
    Print the coefficients for the better accuracy.

    :param results: a matrix with the list of coefficients and accuracy
    """
    best_acc_idx = np.argmax(result, axis=0)[0, -1]
    print('----   Best results (accuracy)   ---')
    print(get_header())
    print('Coefficients: {}'.format(result[best_acc_idx, 0:9]))
    print('Intercept   : {}'.format(result[best_acc_idx, 9]))
    print('Accuracy    : {}'.format(result[best_acc_idx, 12]))
    print('Penalty     : {}'.format(result[best_acc_idx, 10]))
    print('Margin      : {}'.format(result[best_acc_idx, 11]))
    return best_acc_idx


def plot_coefficients(result, feature_names, fn, top_features=10):
    """Plot the coefficients in an ordered bar chart."""
    coef = result[0, 0:9].A1
    top_features = min(len(feature_names), top_features)
    indices = np.argsort(coef)[0:top_features]
    fig, ax = plt.subplots()
    colors = ['lightgray' if c < 0 else 'gray' for c in coef[indices]]
    ax.bar(np.arange(top_features), coef[indices], color=colors)
    ax.set_xticks(np.arange(0, top_features))
    ax.set_xticklabels(tuple(name for name in np.array(feature_names)[indices]))
    plt.tight_layout()
    plt.savefig(fn)


def plot_accuracy(A, fn):
    """Plot the tradeoff between accuracy and margin/loss."""
    A = A.transpose()
    fig, ax = plt.subplots()
    penalties = A[10].getA1()
    ax.set_xlabel('Penalty')
    ax.fill_between(penalties, 100 * A[-1].getA1(), color='lightgray', alpha=0.7, label='Accuracy')
    ax.set_ylabel('Accuracy (\%)')
    ax.set_ylim([0, 100.0])

    ax2 = ax.twinx()
    label = 'Loss' if fn == 'mlp.pdf' else 'Margin'
    ax2.fill_between(penalties, A[-2].getA1(), color='darkgray', alpha=0.7, label=label)
    ax2.set_ylabel(label)
    ax2.set_ylim([0, np.max(A[-2])])

    ax.grid(True)
    plt.tight_layout()
    if fn == 'mlp.pdf':
        plt.gca().invert_xaxis()
        ax.set_xscale('log')
    plt.savefig(fn)


def plot_results(A, fn):
    """Draw a plot to check the evolution of the coefficients."""
    A = A.transpose()
    fig, ax = plt.subplots()
    colors = plt.cm.gray(np.linspace(0.0, 0.9, 9))
    ax.set_prop_cycle(cycler(color='bgrcmyk'))
    penalties = A[10].getA1()
    idxs = np.argsort(A[0:len(names), -1].getA1())[::-1]
    for idx in idxs:
        ax.plot(penalties, A[idx].getA1(), label=names[idx], marker='o')
    c_max = np.ceil(np.max(np.abs(A[:, 0:len(names)])))
    ax.set_ylim([-c_max, c_max])
    ax.set_xlabel('Penalty factor')
    ax.set_ylabel('Coefficient weight')
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
              ncol=3, fancybox=True, shadow=False, framealpha=1.0)
    ax.grid(True)
    plt.tight_layout()
    if fn == 'mlp.pdf':
        plt.gca().invert_xaxis()
        ax.set_xscale('log')
    plt.savefig(fn)


result = []
for c in np.arange(0.1, 1.5, 0.1):
    r = test_svc(c)
    result.append(r)
result = np.matrix(result)

# Print interesting coefficients
print('\n----   Support Vector Machine    ----')
best_acc_idx = print_best(result)
# Plot coefficient evolution wrt penalty parameter
plot_results(result, 'svm.pdf')
plot_accuracy(result, 'svm-acc.pdf')
plot_coefficients(result[best_acc_idx], names, 'svm-coef.pdf')

# Compare with Logistic Regression
result = []
for c in np.arange(1.0, 10.1, 1.0):
    r = test_logreg(c)
    result.append(r)
result = np.matrix(result)

print('\n----   Logistic Regression    ----')
print_best(result)

# Compare with Multi Layer Perceptron
result = []
alphas = np.power(10, np.linspace(-2, 1, 10))
for alpha in alphas:
    r = test_mlp(alpha)
    result.append(r)
result = np.matrix(result)

# Print interesting coefficients
print('\n----   Multi Layer Perceptron   ----')
print_best(result)
