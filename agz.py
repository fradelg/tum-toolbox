import os.path
import csv
import numpy as np
import quaternion
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('gps', help='the GPS file relating timestamps')
    parser.add_argument('gt', help='the ground truth file input')
    parser.add_argument('dst', help='the output path for results (TUM RGBD)')
    parser.add_argument('--limit', type=int, help='the max number of frames')
    args = parser.parse_args()

    timestamps = []
    LIMIT = args.limit if args.limit else 350
    output = os.path.join(args.dst, 'rgb.txt')
    with open(args.gps, 'r') as gti:
        reader = csv.reader(gti, delimiter=',')
        next(reader, None)  # skip the headers
        with open(output, 'w') as gto:
            writer = csv.writer(gto, delimiter=' ')
            for row in reader:
                t = float(row[0]) / 1e7
                timestamps.append(t)
                i = int(row[1])
                if i > LIMIT:
                    break
                writer.writerow([t, 'rgb/{:05d}.jpg'.format(i)])

    # Convert the GT into TUM format: t px py pz qx qy qz qw
    imageCount = 0
    output = '{}.gt.txt'.format(args.dst)
    with open(args.gt, 'r') as gti:
        reader = csv.reader(gti, delimiter=',')
        next(reader, None)  # skip the headers
        with open(output, 'w') as gto:
            writer = csv.writer(gto, delimiter=' ')
            for row in reader:
                a = [float(row[4]), float(row[5]), float(row[6])]
                q = quaternion.from_euler_angles(a[0], a[1], a[2])
                t = timestamps[imageCount]
                data = [t, row[1], row[2], row[3], q.x, q.y, q.z, q.w]
                writer.writerow(data)
                imageCount = imageCount + 1
                if imageCount > LIMIT:
                    break
