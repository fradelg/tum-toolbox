import os
import argparse
import numpy as np
import quaternion
import pykitti

parser = argparse.ArgumentParser(
    description='convert KITTI GT into TUM GT (stamp tx ty tz qx qy qz qw)')
parser.add_argument('dataset', help='path of the KITTI dataset')
parser.add_argument('sequence', help='id of the sequence to process')
parser.add_argument('output', help='output file path')
args = parser.parse_args()

# Load the data
dataset = pykitti.odometry(args.dataset, args.sequence)

# Load dataset timestamps and poses
dataset.load_timestamps()   # Timestamps are parsed into timedelta objects
dataset.load_poses()        # Ground truth poses are loaded as 4x4 arrays

# Convert matrix to quaternion and print to file
np.set_printoptions(precision=8, suppress=True, linewidth=1000)
with open(args.output, "w") as text_file:
    for i in range(len(dataset.T_w_cam0)):
        timestamp = dataset.timestamps[i].total_seconds()
        translation = dataset.T_w_cam0[i][0:3, 3]
        rotation = quaternion.from_rotation_matrix(
            dataset.T_w_cam0[i][0:3, 0:3])
        print("{:10.6f} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f}".format(
            timestamp,
            translation[0], translation[1], translation[2],
            rotation.x, rotation.y, rotation.z, rotation.w),
            file=text_file)
