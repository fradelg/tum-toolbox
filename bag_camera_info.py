#!/usr/bin/env python

import roslib
import rospy
import rosbag
import os
import sys
import argparse
import yaml
import sensor_msgs.msg
import camera_calibration_parsers


def replacement(param):
    pair = param.split('=', 1)
    if len(pair) != 2:
        raise argparse.ArgumentTypeError(
            "Replacement string must have the format /topic=calib_file")

    if pair[0][0] != '/':
        pair[0] = '/' + pair[0]

    rospy.loginfo(
        "Writing {}/camera_info from calibration file {} ...".format(pair[0], pair[1]))

    camera_name, cam_info = camera_calibration_parsers.readCalibration(pair[1])
    return pair[0], cam_info

if __name__ == "__main__":
    rospy.init_node('bag_camera_info')
    parser = argparse.ArgumentParser(
        description='Append a camera_info message linked to image topic in a bag')
    parser.add_argument('inbag', help='input bagfile')
    parser.add_argument('outbag', help='output bagfile')
    parser.add_argument('replacement', type=replacement, nargs='+',
                        help='topic=file, e.g. /my_camera=my_calibration_file')
    args = parser.parse_args()

    try:
        outbag = rosbag.Bag(args.outbag, 'w')
        for topic, msg, t in rosbag.Bag(args.inbag, 'r').read_messages():
            outbag.write(topic, msg, t)
            for base_topic, calib in args.replacement:
                if base_topic == topic.rsplit('/', 1)[0]:
                    new_msg = calib
                    new_msg.header = msg.header
                    outbag.write(base_topic + '/camera_info', new_msg, t)

        rospy.loginfo('Closing output bagfile ...')
        outbag.close()
        rospy.loginfo('Done')
    except Exception, e:
        import traceback
        traceback.print_exc()
