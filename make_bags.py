"""
Create a ROS bag file with the images of a sequence.

The sequences to convert are readed from a txt file (one sequence per line).
We assume that the path of the sequence is the same than the bag.
Besides, the images are saved in a folder rgb inside the sequence's path
"""
import argparse
import img2bag
import os.path

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='a file with the image sequences')
    args = parser.parse_args()
    with open(args.input) as f:
        for sequence in f.readlines():
            sequence = sequence.rstrip('\r\n')
            if not sequence or sequence.startswith('#'):
                continue
            bagfn = '{}.bag'.format(sequence)
            if os.path.isfile(bagfn):
                print('Bag {} already exists. Skipping ...'.format(bagfn))
            else:
                img2bag.CreateBag('{}/rgb'.format(sequence), bagfn)
