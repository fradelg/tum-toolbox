"""Analyze the influence of K in KN-SLAM."""
import warnings
import numpy as np
import matplotlib.pyplot as plt
import argparse
from pathlib import Path

parser = argparse.ArgumentParser(description='Analyze the influence of K')
parser.add_argument('dir', help='path of the directory with the results')
parser.add_argument('start', type=int)
parser.add_argument('stop', type=int)
parser.add_argument('step', type=int)
args = parser.parse_args()

ate = []
ks = list(range(args.start, args.stop + args.step, args.step))
k_path = Path(args.dir)
for k in ks:
    fn = k_path / '{}.csv'.format(k)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        dtype = ('|S13', float, float, float, float, float)
        data = np.genfromtxt(fn, delimiter=',', dtype=dtype)

    if data.size is 0:
        raise RuntimeError('0 samples for {}'.format(self.path))

    ate.append([100.0 * r[4] for r in data])

fig, ax = plt.subplots()
ax.boxplot(ate, labels=ks, showfliers=False)
median = np.median(np.matrix(ate), axis=1)
ax.plot(np.array(ks) / args.step, median.getA1().tolist(), marker='o')
ax.set_xlabel('Maximum number of edges')
ax.set_ylabel('Average Trajectory Error (cm)')
N = len(k_path.parts)
bench = k_path.parts[N-3]
seq = k_path.parts[N-2]
ax.set_title('Analysis of K in {}/{}'.format(bench, seq.replace('_', '\_')))
plt.tight_layout()
plt.savefig(k_path / 'k-{}.pdf'.format(seq.replace('_', '-')))
plt.close()
