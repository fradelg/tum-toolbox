"""Get some stats about 3D delaunay triangulation."""
import argparse
import scipy.spatial as sp
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Analyze the connectivity of 3D Delaunay')
parser.add_argument('dist', help='type of probability distribution')
parser.add_argument('npoints', type=float, help='number of vertices')
parser.add_argument('scale', type=float, help='scale of the coordinates')
args = parser.parse_args()

if args.dist == 'normal':
    vertices = np.random.normal(0.0, args.scale, (int(args.npoints), 3))
elif args.dist == 'uniform':
    vertices = np.random.uniform(0.0, args.scale, (int(args.npoints), 3))

vertices = np.vstack((vertices, [-args.scale, -args.scale, -args.scale]))
vertices = np.vstack((vertices, [-args.scale, -args.scale, +args.scale]))
vertices = np.vstack((vertices, [-args.scale, +args.scale, -args.scale]))
vertices = np.vstack((vertices, [-args.scale, +args.scale, +args.scale]))
vertices = np.vstack((vertices, [+args.scale, -args.scale, -args.scale]))
vertices = np.vstack((vertices, [+args.scale, -args.scale, +args.scale]))
vertices = np.vstack((vertices, [+args.scale, +args.scale, -args.scale]))
vertices = np.vstack((vertices, [+args.scale, +args.scale, +args.scale]))

delaunay = sp.Delaunay(vertices)
(indices, indptr) = delaunay.vertex_neighbor_vertices
neighboors = [len(indptr[indices[i]:indices[i+1]]) for i in range(int(args.npoints))]

print('Mean   : {}'.format(np.mean(neighboors)))
print('Std    : {}'.format(np.std(neighboors)))
print('Q1     : {}'.format(np.percentile(neighboors, 25)))
print('Median : {}'.format(np.median(neighboors)))
print('Q3     : {}'.format(np.percentile(neighboors, 75)))
print('20 is the {} percentile'.format(stats.percentileofscore(neighboors, 20)))

fig, (ax0, ax1) = plt.subplots(1, 2, sharey=True, tight_layout=False)
ax0.hist(neighboors, bins=30, orientation="horizontal", density=True, facecolor='g')
ax0.set_xlabel('Frequency')
ax0.set_ylabel('\# Neighbors')
ax0.set_ylim(0, 40)

ax1.boxplot(neighboors)
ax1.set_xticks([])

# fig.suptitle('Vertex degree in 3D-Delaunay triangulation')
fig.tight_layout()
plt.grid(True)
plt.subplots_adjust(bottom=0.1, wspace=0.3, top=0.9)
plt.savefig('delaunay-{}.pdf'.format(args.dist))
