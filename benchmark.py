"""Benchmark results of several SLAM systems over a specific sequence."""
import os
import sys
import time
import glob
import argparse
import shutil
import yaml
import numpy as np
from Trajectory import Trajectory
from Sequence import Sequence


# Custom Exception
class NoKFsError(Exception):
    """The Trajectory does not contain any keyframe."""


def file_len(fname):
    """Count the number of lines in a text file."""
    if not os.path.isfile(fname):
        return 0

    with open(fname) as f:
        content = f.readlines()
    return len(content)


def rm_file(fn):
    """Remove a file if exists."""
    try:
        os.remove(fn)
    except OSError:
        pass


def outliers_mod_z_score(ys):
    """Determine outliers using the modified Z-score method."""
    threshold = 3.5
    median_y = np.median(ys)
    median_abs_dev_y = np.median([np.abs(y - median_y) for y in ys])
    z_scores = [0.6745 * (y - median_y) / median_abs_dev_y for y in ys]
    return np.where(np.abs(z_scores) > threshold)


def query_yes_no(question, default="no"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def test_sequence(seq, slam, timestamp):
    """
    Test a specific SLAM system for a sequence.

    :param seq: the sequence data to test
    :param slam: the slam label for the method to test
    :param timestamp: the stamp to identify the slam experiment
    :return: the path of the trajectory of keyframes
    """
    slam_path = seq.slam_path(slam)

    # Create dir to allocate results if not exists
    os.makedirs(slam_path, exist_ok=True)

    # Benchmark the sequence with the SLAM system linked to the label
    seq.run(slam)

    # subprocess.call(['roslaunch', PATH_SLAM, '{}.launch'.format(PATH_SLAM),
    #                  'image:={}'.format(topic),
    #                  'bags:={}.bag'.format(seq),
    #                  'settings:=/opt/ros/share/{}/data/{}.yml'.format(PATH_SLAM, settings)])

    # Count the total number of KF
    tr_fn = '/tmp/trajectory.txt'
    kfs = file_len(tr_fn)
    if kfs is 0:
        raise NoKFsError('No keyframes for {}'.format(seq.path))

    # Move the results to their own path
    tr_kf = slam_path / '{}.tr.txt'.format(timestamp)
    shutil.move(tr_fn, tr_kf)

    # Move the histogram of edges
    hf_fn = '/tmp/histogram.txt'
    if os.path.isfile(hf_fn):
        dst = slam_path / '{}.he.txt'.format(timestamp)
        shutil.move(hf_fn, dst)

    # Move the map
    mf_fn = '/tmp/map.ply'
    if os.path.isfile(mf_fn):
        dst = slam_path / '{}.mp.ply'.format(timestamp)
        shutil.move(mf_fn, dst)

    # Move the graph
    gr_fn = '/tmp/poses.gv'
    if os.path.isfile(gr_fn):
        dst = slam_path / '{}.gv'.format(timestamp)
        shutil.move(gr_fn, dst)

    return eval_sequence(seq, slam, tr_kf)


def eval_sequence(seq, slam, tr_fn):
    """Evaluate the results for a sequence using a trajectory file."""
    # Extract date from filename
    _, fn = os.path.split(tr_fn)
    date = fn[0:fn.index('.')]

    # Align the trajectory wrt the ground truth in the sequence
    trf = Trajectory(tr_fn, slam)
    gt, tr = seq.align_gt(trf)

    # Compute the ATE (Average Trajectory Error)
    rmse = tr.rmse(gt)
    rmse_q = tr.rmse_q(gt)

    # Compute the PTT (Percentage of Tracked Trajectory)
    ptt = tr.tracked() / seq.duration

    return (date, tr.length, tr.edges, ptt, rmse, rmse_q)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Benchmark a TUM sequence')
    parser.add_argument('seq_path', help='path of the benchmark (rgb.txt)')
    parser.add_argument('slam', help='label of the SLAM system to test')
    parser.add_argument('times', type=int, help='number of executions')
    parser.add_argument('--overwrite', dest='overwrite', action='store_true')
    args = parser.parse_args()

    if args.overwrite:
        if not query_yes_no('[WARNING] Any previous experiment will be deleted!'):
            exit()

    seqs = []  # List of sequences for benchmarking
    fn, ext = os.path.splitext(args.seq_path)
    if ext == ".yml" or ext == ".yaml":
        with open(args.seq_path) as f:
            doc = yaml.load(f)
            for seq_data in doc['sequences']:
                seqs.append(Sequence(seq_data))
    else:
        seqs.append(Sequence({'path': args.seq_path}))

    for seq in seqs:
        res_path = seq.slam_path(args.slam)

        if args.overwrite:
            try:
                shutil.rmtree(res_path)
                os.makedirs(res_path, exist_ok=True)
                seq.truncate(args.slam)
            except OSError:
                pass
        else:
            tr_list = glob.glob('{}/*.tr.txt'.format(res_path))
            if len(tr_list) > 0:
                print('Existing experiments for {}. Skipping ...'.format(seq.path))
                continue

        # Benchmark this sequences with the SLAM system
        for i in range(args.times):
            try:
                date = time.strftime("%Y%m%d-%H%M%S")
                print('Running the {}-th experiment for {}'.format(i, seq.path))
                d = test_sequence(seq, args.slam, date)
                seq.append_result(args.slam, d)
                print('Results: {}'.format(d))
            except NoKFsError:
                # System does not initialize
                pass
            except RuntimeError as error:
                # Something worse happened
                print(error)
                break
