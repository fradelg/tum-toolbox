#!/usr/bin/env python3
"""Run and eval a binary from QtCreator."""
import sys
import os
import subprocess
import evaluate_ate as ate
import Sequence

if __name__ == "__main__":
    bin = os.environ.get('SLAM_BIN')
    seq = Sequence.Sequence({'path': sys.argv[1]})
    tr = '/tmp/trajectory.txt'
    voc = 'data/orb-voc.proto'

    # redirect null to stdout to capture the SLAM output
    stdout = sys.stdout
    sys.stdout = open(os.devnull, 'w')

    # invoke the SLAM binary showing the GUI
    proc = subprocess.call([bin, voc, seq.settings_path, seq.path, 'gui'])
    sys.stdout = stdout
    ate.eval(seq.gt_path, tr)
