"""Handle ASL (EuRoC) datasets."""
import os
import argparse
import tempfile
import zipfile
import shutil
import StringIO
import urllib
import csv

import asl2tum
import img2bag

gtPath = 'mav0/state_groundtruth_estimate0/data.csv'
imPath = 'mav0/cam0/data'


def ParseDataset(args):
    """Download and extract images, trajectory and GT from ASL format."""
    if args.input.startswith("http"):
        print('downloading content, it may take a while ...')
        url = urllib.urlopen(args.input)
        zfile = zipfile.ZipFile(StringIO.StringIO(url.read()))
    else:
        print('opening compressed file ...')
        zfile = zipfile.ZipFile(args.input)

    # extract content from file in ASL format
    print('extracting images and ground truth for sequence {}'.format(args.sequence))
    imgDir = tempfile.mkdtemp()
    for fs in zfile.namelist():
        if fs.startswith(imPath):
            zfile.extract(fs, imgDir)

    # extract ground truth file
    tmpFile = os.path.join(imgDir, 'poses.csv')
    with open(tmpFile, 'w') as f:
        f.write(zfile.read(gtPath))
    zfile.close()

    # convert camera poses to the TUM format
    print('converting groundtruth from {}'.format(tmpFile))
    poses = asl2tum.parse_TUM(tmpFile)
    gtfn = os.path.join(args.output, '{}.gt.txt'.format(args.sequence))
    asl2tum.write_TUM(poses, gtfn)

    # create a bag with image sequence
    print('creating a bag of images from {}'.format(imgDir))
    bagfn = os.path.join(args.output, '{}.bag'.format(args.sequence))
    img2bag.CreateBag(os.path.join(imgDir, imPath), bagfn)

    # cleanup
    print('cleaning tmp dir {} content'.format(imgDir))
    shutil.rmtree(imgDir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''extract ground truth data and bag from ASL''')
    parser.add_argument('input', help='URI of an ASL zipped dataset')
    parser.add_argument('output', help='path of the GT and bag output')
    parser.add_argument('--sequence', help='the sequence ID used for file naming')
    args = parser.parse_args()

    # download a set of dataset listed in a txt file
    if os.path.splitext(args.input)[1] == '.csv':
        with open(args.input) as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                args.sequence = row[0].strip()
                args.input = row[1].strip()
                ParseDataset(args)
    else:
        ParseDataset(args)
