"""Utils to plot comparisons between connectivity graph edge stats."""
import numpy as np
import matplotlib
# matplotlib.use('Agg')  # hide window


def plot_cmp(freq_file, freq_ref_file, output_file):
    """Plot the frequency histograms of weighted edges."""
    freqs = np.loadtxt(freq_file)
    if freq_ref_file:
        refs = np.loadtxt(freq_ref_file)
    weight = [i for i in range(len(freqs))]

    # Draw the plot to compare
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    ax.yaxis.grid(True)
    ax.set_xlabel('Shared observations')
    ax.set_ylabel('Frequency')
    ax.set_ylim([0, np.max(freqs)])
    if freq_ref_file:
        ax.bar(weight, refs, label='ORB-SLAM', color='g', alpha=0.9, width=1.0)
    ax.bar(weight, freqs, label='Our SLAM', color='r', alpha=0.5, width=1.0)
    ax.legend(loc='best', fontsize='12', fancybox=True, framealpha=0.5)
    plt.savefig(output_file, dpi=90)
    plt.close()


def count(fq_fn):
    """Return the total number of edges."""
    return int(np.sum(np.loadtxt(fq_fn)))


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='the ascii file with the histogram')
    parser.add_argument('plot', help='filename of the output plot')
    args = parser.parse_args()
    plot_cmp(args.input, "", args.plot)
    print('Total count of edges: {}'.format(count(args.input)))
