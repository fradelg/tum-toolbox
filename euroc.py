"""Download, parse and convert EuRoC sequences into TUM RGB-D format."""
import os.path
import csv
import subprocess
import icl


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='the file with datasets')
    args = parser.parse_args()

    datasets = icl.parseDataset(args.input)
    path = os.path.dirname(args.input)

    for dataset in datasets:
        dst = os.path.join(path, dataset[0])
        if os.path.exists(dst):
            print('path {} already there; go to next sequence'.format(dst))
            continue

        # Download the dataset from URL
        icl.download(dataset[1], path)

        # Extract only cam0 data
        filename = dataset[1].rsplit('/', 1)[-1]
        icl.unzip(os.path.join(path, filename), 'mav0/cam0', dst)
        icl.execute('mv {}/mav0/cam0/data {}'.format(dst, dst))
        icl.execute('mv {}/mav0/cam0/data.csv {}'.format(dst, dst))
        icl.execute('rm -rf {}'.format(os.path.join(dst, 'mav0')))

        datafile = os.path.join(dst, 'data.csv')
        rgbfile = os.path.join(dst, 'rgb.txt')
        with open(datafile, 'r') as f:
            reader = csv.reader(f, delimiter=',')
            with open(rgbfile, 'w') as fo:
                writer = csv.writer(fo, delimiter=' ')
                for row in reader:
                    if row[0][0] == '#':
                        continue
                    writer.writerow([float(row[0]) / 1e9, 'data/' + row[1]])

        icl.execute('rm {}'.format(os.path.join(dst, 'data.csv')))
