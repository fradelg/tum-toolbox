"""Run experiments and create a summary file with stats for each sequence."""
import numpy as np
import argparse
import os
import time
import sys
import yaml
import benchmark
from Sequence import Sequence
from Trajectory import Trajectory


if __name__ == "__main__":
    np.set_printoptions(precision=4, suppress=True)
    parser = argparse.ArgumentParser()
    parser.add_argument('sequences', help='a file with the image sequences')
    parser.add_argument('--times', type=int, default=1,
                        help='number of executions')
    parser.add_argument('--overwrite', action='store_true',
                        help='overwrite previous results')
    args = parser.parse_args()

    if args.overwrite:
        if not benchmark.query_yes_no("[WARNING] Delete any previous result?"):
            exit()

#    rs_fn = 'results-{}.csv'.format(time.strftime("%Y%m%d-%H%M"))
    rs_fn = 'results.csv'
    slams = ['kn', 'orb']

    # Write a header for results file
    with open(rs_fn, 'w') as fr:
        header = ['id', 'ben', 'seq', 'dx', 'dy', 'dz']
        for s in slams:
            row = ['n', 'k', 'e', 'c', 'p', 'q', 's']
            header += ['{}{}'.format(s, r) for r in row]
        fr.write(', '.join(header) + '\n')

    # Read sequence list from YAML file
    with open(args.sequences) as f:
        doc = yaml.load(f)
        seq_id = 0
        goods = {s: [0.0, 0.0, 0.0] for s in slams}
        firsts = []
        for seqd in doc['sequences']:
            seq = Sequence(seqd)

            # Test the sequence multiple times for each SLAM system alternative
            print('Performing experiments for {} ...'.format(seq.path))
            times = []
            first = []
            for slam in slams:
                # Count experiments for each slam method
                tr_list = seq.tr_glob(slam)
                N = len(tr_list)
                times.append(N)

                # Recompute stats from trjs for this sequence
                if args.overwrite:
                    seq.truncate(slam)
                    for fn in tr_list:
                        d = benchmark.eval_sequence(seq, slam, fn)
                        seq.append_result(slam, d)

                # Determine the first tracked frame for each trajectory
                idx = 0
                for fn in tr_list:
                    trj = Trajectory(fn)
                    idx += seq.search_first_frame(trj)
                first.append(idx / N if N > 0 else seq.length)
            firsts.append(first)

            # Benchmark sequences only if all slams are empty
            if np.sum(times) == 0.0:
                for i in range(args.times):
                    date = time.strftime("%Y%m%d-%H%M%S")
                    for slam in slams:
                        try:
                            d = benchmark.test_sequence(seq, slam, date)
                            seq.append_result(slam, d)
                        except benchmark.NoKFsError as error:
                            # System does not initialize
                            pass
                        except RuntimeError as error:
                            # Something worse happened
                            print(error)
                            break

            # Determine the short ID for this sequence
            row = ['{}'.format(seq_id), seq.benchmark, seq.name]

            # Compute the dimensions of the sequence
            dims = Trajectory(seq.gt_path).size
            row += ['{:2.6f}'.format(d) for d in dims]

            # Load results from multiple iterations on every SLAM system
            for slam in slams:
                try:
                    seq.load_res(slam)
                except Exception as error:
                    print(error)
                    # Handle the case when no results have been obtained
                    print('No results for {} in {}'.format(slam, seq.path))
                    row += ['0', '0', '0', '0.0', '0.0', '0.0', '0.0']
                    continue

                # Compute the mean PTT and the median ATE
                mp = seq.cover(slam, 3)  # 3th column holds PTT
                _, n, m = seq.ate(slam, 4)  # 4th column holds ATE

                # Compute the STD for the 5 central values
                inliers = seq.get_inliers(slam, 5)
                std = np.std(inliers)
                mean = np.mean(inliers)

                # Compute the percentual relation between ATE and STD
                goods[slam][0] += 1
                goods[slam][1] += 100 * std / mean
                if std < 0.5 * mean:
                    goods[slam][2] += 1

                # Print warning is sequence has not enough samples
                if n < args.times:
                    print('{}:{} has {} samples'.format(slam, seq.path, n))

                # Append the estimations to the row of results
                row.append(str(n))
                row += ['{:.0f}'.format(m[i]) for i in range(0, 2)]
                row.append(mp[1])
                row += ['{:2.6f}'.format(m[i]) for i in range(3, 5)]
                row.append('{:2.6f}'.format(std))

            # Write results for this sequence to the summary file
            with open(rs_fn, 'a') as fr:
                fr.write(', '.join(row) + '\n')
            seq_id += 1

    # Show results of the dispersion analysis
    for s in slams:
        print('{}: {} / {} seqs in 10%'.format(s, goods[s][2], goods[s][0]))
        print('i.e. {:2.6f}% of the seqences'.format(100.0 * goods[s][2] / goods[s][0]))
        print('{:2.6} of std as % of the mean'.format(goods[s][1] / goods[s][0]))

    # Show results concerning first frame initialization
    score = [0, 0, 0]
    for idx, s in enumerate(firsts):
        winner = 2
        if s[0] < s[1]:
            winner = 0
        elif s[1] < s[0]:
            winner = 1
        score[winner] += 1
    print('First frame initilization:')
    print('KN: {}, ORB: {}, EQUAL: {}'.format(score[0], score[1], score[2]))
