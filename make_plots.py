"""Plot different comparisons between results for different SLAMs."""
import argparse
import os
import yaml
from Trajectory import Trajectory
from Sequence import Sequence

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sequences', help='a file with the sequence list')
    parser.add_argument('--sequence', help='the path to the sequence')
    parser.add_argument('--slams', nargs='+', help='IDs of slam to plot')
    args = parser.parse_args()

    if args.sequence is not None:
        Sequence({'path': args.sequence}).plot(args.slams)
    elif args.sequences is not None:
        # Read sequence list from YAML file
        with open(args.sequences) as f:
            doc = yaml.load(f)
            benchs = {}
            for seqd in doc['sequences']:
                seq = Sequence(seqd)
                # seq.plot(args.slams)

                # Cluster sequences by benchmark
                bench = seq.benchmark
                if bench in benchs:
                    benchs[bench].append(seq)
                else:
                    benchs[bench] = [seq]

        # Plot the box for dispersion analysis
        for bench, seqs in benchs.items():
            seq = seqs[0]  # Take the 1st sequence
            fn = seq._path.parent / '{}.pdf'.format(bench)
            seq.plot_box(args.slams, str(fn), seqs)

        # Split the hline + marker plot for kitti and rest sequences
        kitti = benchs['kitti']
        rest = benchs['tum'] + benchs['euroc'] + benchs['icl']
        rest[0].plot_bar(args.slams, 'ate-rest.pdf', rest, 'kn')
        kitti[0].plot_bar(args.slams, 'ate-kitti.pdf', kitti, 'kn')
