"""
Computes the absolute trajectory error.

The ground truth trajectory and the estimated trajectory are compared.
"""

import os
import sys
import numpy as np
import quaternion
import argparse
import associate as tum
import Sequence
from Trajectory import Trajectory


def eval(gtfn, trfn, max_diff=0.02, offset=0.0):
    """
    Eval the ATE and ARE between a pair of trajectories.

    :param gtfn: filename of the ground truth
    :param trfn: filename of the estimated trajectory
    :param max_diff: maximum distance between consecutive poses
    :param offset: time offset difference between both sets of poses
    """
    # Align the sequence with the ground truth
    gtf = Trajectory(gtfn, 'gt')
    trf = Trajectory(trfn, 'slam')
    gt, tr = Sequence.align_trjs(gtf, trf, max_diff, offset)
    print("ATE = {:f}".format(tr.rmse(gt)))
    print("ARE = {:f}".format(tr.rmse_q(gt)))


if __name__ == "__main__":
    # parse command line arguments
    parser = argparse.ArgumentParser(description='Compute ATE from ground truth and trajectory.')
    parser.add_argument('first_file', help='ground truth (timestamp tx ty tz qx qy qz qw)')
    parser.add_argument('second_file', help='trajectory (timestamp tx ty tz qx qy qz qw)')
    parser.add_argument('--offset', help='time offset for timestamps of second file', default=0.0)
    parser.add_argument(
        '--max-difference',
        help='max difference for matching entries (default: 0.02)',
        default=0.02)
    parser.add_argument(
        '--save',
        help='save aligned second trajectory to disk (stamp2 x2 y2 z2)'
    )
    parser.add_argument(
        '--save_associations',
        help='save associated aligned trajectory to disk (stamp1 x1 y1 z1 stamp2 x2 y2 z2)'
    )
    parser.add_argument(
        '--plot',
        help='plot the first and the aligned second trajectory to an image (format: png)'
    )
    parser.add_argument(
        '--plot-error',
        help='whether the difference in the trajectory should be plotted or not',
        action='store_true')
    parser.add_argument(
        '--swap-yz',
        help='whether the Y and Z axis should be swap in the plot',
        action='store_true')
    parser.add_argument(
        '--three',
        help='whether the plot must be three dimensional or not',
        action='store_true')
    parser.add_argument(
        '--verbose',
        help='print all evaluation data',
        action='store_true')
    args = parser.parse_args()

    eval(args.first_file, args.second_file, args.max_difference, args.offset)

    if args.verbose:
        print("compared_pose_pairs {:d} pairs".format(len(error_t[0])))
        print("ate.rmse {:f} m".format(
            np.sqrt(np.dot(error_t[0], error_t[0]) / len(error_t[0]))))
        print("ate.mean {:f} m".format(np.mean(error_t[0])))
        print("ate.median {:f} m".format(np.median(error_t[0])))
        print("ate.std {:f} m".format(np.std(error_t[0])))
        print("ate.min {:f} m".format(np.min(error_t[0])))
        print("ate.max {:f} m".format(np.max(error_t[0])))

    if args.save_associations:
        with open(args.save_associations, "w") as file:
            file.write("\n".join([
                "%f %f %f %f %f %f %f %f" % (a, x1, y1, z1, b, x2, y2, z2)
                for (a, b), (x1, y1, z1), (x2, y2, z2) in zip(
                    matches, gt[1].transpose().A, slam[1].transpose().A)
            ]))

    if args.save:
        with open(args.save, "w") as file:
            file.write("\n".join([
                "%f " % stamp + " ".join(["%f" % d for d in line])
                for stamp, line in zip(slam[0], slam[2].transpose().A)
            ]))

    if args.plot:
        import matplotlib
        matplotlib.use('Agg')
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(1)
        gt.plot(ax)
        tr.plot(ax)
        if args.plot_error:
            tr.plot_diff(ax, gt)
        gt.adjust_ax(ax)
        plt.savefig(args.plot, dpi=90)
        plt.close()
