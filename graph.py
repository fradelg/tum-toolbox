"""Analyze the connectivity graph of a sequence."""
import argparse
import numpy as np
import networkx as nx
from pathlib import Path
from networkx.drawing.nx_pydot import *

parser = argparse.ArgumentParser(description='Analyze the influence of K')
parser.add_argument('path', help='path of the DOT file with the graph')
args = parser.parse_args()

path = Path(args.path)
G = nx.DiGraph(read_dot(str(path)))
print(nx.info(G))
print('Most repeated degree: {}'.format(np.argmax(nx.degree_histogram(G))))

#for e, w in nx.get_edge_attributes(G, 'weight'):
#    print('Averaged neighboor degree: {}'.format(w))

print('Out degree centrality: {}'.format(nx.out_degree_centrality(G)))
