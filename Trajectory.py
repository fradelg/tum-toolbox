"""A trajectory composed by a set of ketframes and its poses."""
import os
import sys
import numpy as np
import quaternion
import argparse
import associate as tum


def round_half(data):
    """Round to the closest half (0.5) all the members of data."""
    p2 = []
    for v in data:
        p2.append(0.5 * np.ceil(2.0 * v))
    return tuple(p2)


def get_label(slam):
    """Get a human-readable label from a slam key."""
    return {
        'gt': 'Ground Truth',
        'kn': 'KN-SLAM',
        'fra': 'KN-SLAM',
        'orb': 'ORB-SLAM',
        'ptam': 'PTAM',
        'slam': 'SLAM',
    }.get(slam, None)


def get_style(slam):
    """
    Get the matplotlib Line2D properties for a given slam key.

    :param slam: the SLAM method key used as ID
    """
    style = {
        'label': get_label(slam),
        'linestyle': 'solid',
        'markersize': 4.0,
        'linewidth': 1.0,
    }
    if slam == 'gt':
        style['color'] = 'lightgray'
        style['linewidth'] = 4.0
    elif slam == 'orb':
        style['color'] = 'grey'
        style['linestyle'] = 'dashdot'
        style['marker'] = 'D'
    elif slam == 'kn':
        style['color'] = 'black'
        style['linestyle'] = 'dotted'
        style['marker'] = 'o'
    return style


class Trajectory(object):
    """A trajectory of keyframes represented by a pose and a timestamp."""

    def __init__(self, fn='', slam='gt', ts=None):
        """
        Build a new trajectory.

        :param fn: the path of the file with the poses
        :param slam: the ID of the SLAM method
        :param ts: a list of prefiltered timestamps
        """
        self.t = []
        self.p = []
        self.q = []
        self.D = 2
        self.style = get_style(slam)
        if os.path.isfile(fn):
            self.fn = fn
            poses = tum.read_file_list(fn)
            if ts is None:
                self.t = list(poses)
                self.t.sort()
            else:
                self.t = ts
                not_matched = set(poses.keys()) - set(self.t)
                for k in not_matched:
                    del poses[k]

            self.p = np.matrix([[float(v) for v in p[0:3]]
                                for t, p in poses.items()]).transpose()
            self.q = np.matrix([[float(p[i]) for i in (6, 3, 4, 5)]
                                for t, p in poses.items()])
            self.q = quaternion.as_quat_array(self.q)

    def filter_ts(self, ts):
        """Remove data from a set of poses."""
        idxs = [idx for idx, t in enumerate(self.t) if t not in ts]
        self.t = ts
        self.p = np.delete(self.p, idxs, 1)
        self.q = np.delete(self.q, idxs)

    @property
    def length(self):
        """Get the total number of keyframes of the trajectory."""
        return len(self.t)

    @property
    def edges(self):
        """Get the total number of edges."""
        if self.fn is None:
            raise RuntimeError('There is no filename linked to trajectory')

        fn = str(self.fn).replace('.tr.txt', '.he.txt')
        if not os.path.isfile(fn):
            return 0
        return int(np.sum(np.loadtxt(fn)))

    @property
    def dims(self):
        """Compute the variance of the L2 distance between consecutive poses."""
        N = self.p.shape[1]
        D0 = self.p[:, 0:N - 1]
        D1 = self.p[:, 1:N]
        E = D1 - D0
        return np.var(np.sqrt(np.sum(np.multiply(E, E), axis=0)))

    @property
    def bbox(self):
        """Retrieve the bounds of a 3D trajectory."""
        minimun = self.p.min(axis=1).transpose().A[0]
        maximum = self.p.max(axis=1).transpose().A[0]
        return minimun, maximum

    @property
    def size(self):
        """Get the dimensions of this trajectory."""
        bb_min, bb_max = self.bbox
        return bb_max - bb_min

    def plot_p(self, gt, ax):
        """
        Plot the translational error at each keyframe of this trajectory.

        :param gt: the ground truth trajectory or reference to compare.
        :param ax: the axis object to draw the plot.
        """
        # Compute the RMSE by keyframes
        Ep = np.sqrt(np.sum(np.square(self.p - gt.p), 0)).A[0]
        rmse = np.sqrt(np.dot(Ep, Ep) / len(Ep))
        maxe = np.argmax(Ep)
        # print('e_max at t={} is {}'.format(self.t[maxe], Ep[maxe]))

        # Plot the translational error
        ax.plot(self.t, Ep, **self.style)
        ax.axhline(y=rmse, color='b')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('translation (m)')

    def plot_q(self, gt, ax):
        """
        Plot the rotational error between this trajectory and the GT.

        :param gt: the ground truth to compare.
        :param ax: the axis object to plot the data.
        """
        # Compute the rotational error using quaternion dot product
        # Er = np.abs(np.sum(np.multiply(self.q, gt.q), 0)) - 1.0
        Er = np.array([quaternion.rotation_intrinsic_distance(r1, r2)
                       for r1, r2 in zip(self.q, gt.q)])

        # Plot the E_r(t)
        ax.plot(self.t, np.rad2deg(Er), **self.style)
        ax.set_xlabel('time (s)')
        ax.set_ylabel('rotational error (degrees)')

    def plot_qa(self, gt, ax):
        """
        Plot the accumulated rotational error between this trajectory and the GT.

        :param gt: the ground truth to compare.
        :param ax: the axis object to draw the plot.
        """
        if len(self.q) != len(gt.q):
            raise ValueError('trajectories not aligned: {} vs {}'.format(len(self.q), len(gt.q)))

        # Compute the accumulated ARE along the keyframes of the trajectory
        Er = np.array([quaternion.rotation_intrinsic_distance(r1, r2)
                       for r1, r2 in zip(self.q, gt.q)])
        Ers = np.cumsum(np.rad2deg(Er))
        Era = [(a / (1+idx)) for idx, a in enumerate(Ers.tolist())]

        ax.fill_between(self.t, 0.0, Era, alpha=0.9, label='ARE')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('accumulated rotational error (degrees)')

    def get_Sim3(self, trj):
        """
        Align this trajectory with trj using the method of Horn (closed-form).

        :param trj: 2nd trajectory
        :return: rotation (3x3), translation (3x1), scale (1x1)
        """
        model_centered = self.p - self.p.mean(1)
        data_centered = trj.p - trj.p.mean(1)

        W = np.zeros((3, 3))
        for c in range(model_centered.shape[1]):
            W += np.outer(model_centered[:, c], data_centered[:, c])

        U, d, Vh = np.linalg.linalg.svd(W.transpose())
        S = np.matrix(np.identity(3))
        if np.linalg.det(U) * np.linalg.det(Vh) < 0:
            S[2, 2] = -1
        R = U * S * Vh

        # Compute scale factor between models for Sim(3)
        dots = 0.0
        norms = 0.0
        model_rotated = R * model_centered
        for p in range(data_centered.shape[1]):
            dots += np.dot(data_centered[:, p].transpose(), model_rotated[:, p])
            normi = np.linalg.norm(model_centered[:, p])
            norms += normi * normi

        s = float(dots / norms)
        T = trj.p.mean(1) - s * R * self.p.mean(1)
        return s, R, T

    def apply_Sim3(self, s, R, T, q0=None):
        """
        Apply Sim(3) to this trajectory.

        :param s: scale (scalar)
        :param R: rotation (3x3)
        :param T: rotation (1x3)
        :param q0: the orientation diff between first poses
        """
        self.s = s
        self.R = R
        self.T = T
        self.p = self.s * self.R * self.p + self.T
        if q0 is not None:
            self.q = [q0 * q for q in self.q]

    def rmse(self, trj):
        """
        Compute the positional RMSE between trj and this.

        :param trj: the other trajectory.
        """
        e_p = self.p - trj.p
        e_p = np.sqrt(np.sum(np.multiply(e_p, e_p), 0)).A[0]
        return np.sqrt(np.dot(e_p, e_p) / len(e_p))

    def rmse_q(self, trj):
        """
        Compute the RMSE of orientation between trj and this trajectory.

        :param trj: the other trajectory to compare the orientations
        """
        e_q = np.array([quaternion.rotation_intrinsic_distance(r1.normalized(), r2.normalized())
                        for r1, r2 in zip(self.q, trj.q)])
        e_q = np.rad2deg(e_q)
        return np.sqrt(np.dot(e_q, e_q) / len(e_q))

    def plot_diff(self, gt, ax):
        """
        Plot differences between positions of two trajectories.

        :param gt: the ground truth trajectory
        :param ax: the axis object to plot on
        """
        for i in range(0, len(self.t)):
            p1 = self.p[:, i]
            p2 = gt.p[:, i]
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]], '-', color="red")

    def plot(self, ax):
        """
        Plot the trajectory using lines between contigous positions.

        :param ax: the axis object of the plot
        """
        self.t.sort()
        interval = np.median([s - t for s, t in zip(self.t[1:], self.t[:-1])])
        label_bk = self.style['label']
        poses = []
        for i in range(1, len(self.t)):
            if self.t[i] - self.t[i - 1] < 1.0:
                poses.append(i)
            elif len(poses) > 0:
                self.plot_poses(ax, poses)
                self.style['label'] = ''
                poses = []

        if len(poses) > 0:
            self.plot_poses(ax, poses)

        self.style['label'] = label_bk

        # Mark the beginning and end of the trajectory
        # plotLine(ax, begin, 'o', color, '', lineWidth, swapYZ, three)
        # plotLine(ax, end, 'x', color, '', lineWidth, swapYZ, three)

    def plot_poses(self, ax, poses):
        """
        Plot just a subset of keyframes in the trajectory.

        :param ax: a plot axis object
        :param poses: a list of indices to selected keyframes
        """
        x = self.p[0, poses].A[0]
        y = self.p[1, poses].A[0]
        z = self.p[2, poses].A[0]
        if 'kitti' in str(self.fn):
            ax.plot(x, z, **self.style)
        elif self.D == 3:
            ax.plot(x, y, z, **self.style)
        else:
            ax.plot(x, y, **self.style)

    def adjust_ax(self, ax):
        """Perform som adjustments to ax objects."""
        ax.axis('equal')  # axis have the same dimension
        axisy = 2 if 'kitti' in str(self.fn) else 1
        minp = np.amin(self.p, axis=1)[:, 0]
        maxp = np.amax(self.p, axis=1)[:, 0]
        offset = 0.1 * np.amax(maxp - minp)
        xlim = round_half((minp[0] - offset, maxp[0] + offset))
        ylim = round_half((minp[axisy] - offset, maxp[axisy] + offset))
        ax.set_xticks(np.arange(xlim[0], xlim[1], .5))
        ax.set_yticks(np.arange(ylim[0], ylim[1], .5))
        ax.set_xlabel('x (m)')
        ax.set_ylabel('y (m)' if axisy == 1 else 'z (m)')
        ax.legend(loc='best', fontsize='12', fancybox=True, framealpha=0.5)

    def plot_map(self, ax, map_fn):
        """Plot the map points projection placed along the trajectory."""
        from plyfile import PlyData, PlyElement
        plydata = PlyData.read(map_fn)
        mapdata = plydata['vertex']
        pts = np.matrix([mapdata['x'], mapdata['y'], mapdata['z']])
        pts = self.s * self.R * pts + self.T
        ptsy = pts[2] if 'kitti' in str(self.fn) else pts[1]

        self.plot(ax)
        ax.scatter([pts[0]], [ptsy], marker='o', s=0.01)
        self.adjust_ax(ax)

    def duration(self):
        """Return the time length of this trajectory from first to last."""
        return float(self.t[len(self.t)-1]) - float(self.t[0])

    def tracked(self, N=5):
        """
        Return the total tracked time of this trajectory.

        We use the IQR in the best N time intervals between consecutive
        keyframes as the threshold for outlier rejection.

        :param N: the number of longer durations to consider for threshold
        """
        ts = np.diff(self.t)
        q1, q3 = np.percentile(np.sort(ts)[-N:], [25, 75])
        ub = q3 + 3.0 * (q3 - q1)
        inliers = np.where(ts < ub)[0]
        ts = np.sum(ts[inliers])
        return ts
