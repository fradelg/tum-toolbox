"""
Utilities to pack a set of images into a ROS bag.

Use OpenCV2 for reading and ROS python bindings for archiving.
"""
import time
import sys
import os
from ros import rosbag
import roslib
import rospy
import sensor_msgs.msg
import cv2


def GetFilesFromDir(path):
    """
    Generate a list of files from a directory.

    Parameters
    ----------
    path: String
        The path of the directory containing the images.

    """
    print("listing images from directory {}".format(path))
    all = []
    left_files = []
    right_files = []
    if os.path.exists(path):
        for dirn, names, files in os.walk(path):
            for f in files:
                if os.path.splitext(f)[1] in ['.bmp', '.png', '.jpg', '.ppm']:
                    all.append(f)
                    if 'left' in f or 'left' in dirn:
                        left_files.append(f)
                    elif 'right' in f or 'right' in dirn:
                        right_files.append(f)

    return path, all, left_files, right_files


def CreateImageMsg(cvim):
    """
    Generate a ROS image sensor message from a CV image.

    Parameters
    ----------
    cvim: CvImage
        The OpenCV image to embed into a message.

    """
    img_msg = sensor_msgs.msg.Image()
    img_msg.width = cvim.shape[1]
    img_msg.height = cvim.shape[0]
    img_msg.encoding = 'mono8' if cvim.shape[2] < 3 else 'bgr8'
    if cvim.dtype.byteorder == '>':
        img_msg.is_bigendian = True
    img_msg.data = cvim.tostring()
    img_msg.step = len(img_msg.data) / img_msg.height
    return img_msg


def CreateMonoBag(path, imgs, bagname, framerate=1):
    """
    Create a bag file with camera images.

    The timestamp for each image message is extracted from the filename.

    Parameters
    ----------
    path: String
        The path of the directory containing the images
    imgs : [String]
        The list of filenames to include in the bag
    bagname : String
        The filename (with path) of the resulting bag
    framerate : Float
        A scale to divide timestamps (according to the desired fps)

    """
    # bag = rosbag.Bag(bagname, 'w', compression='bz2')
    bag = rosbag.Bag(bagname, 'w')
    ext = os.path.splitext(imgs[0])[1]
    times = [float(os.path.splitext(f)[0]) for f in imgs]
    times_sorted = sorted(times)

    try:
        for ts in times_sorted:
            fn = '{:f}{}'.format(ts, ext)
            img = cv2.imread(os.path.join(path, fn), cv2.IMREAD_UNCHANGED)
            msg = CreateImageMsg(img)
            t = float(ts) / framerate
            stamp = rospy.rostime.Time.from_sec(t)
            msg.header.stamp = stamp
            msg.header.frame_id = "camera"
            bag.write('camera/image_raw', msg, stamp)
    except RuntimeError as e:
        print('error reading image from {}'.format(fn))
        print(e)
        raise
    finally:
        bag.close()


def CreateBag(input, ouput):
    """
    Create a ROS bag by adding images from a directory.

    Parameters
    ----------
    input : String
        The path of the directory containing the images
    output : String
        The path of the bag file to create

    """
    path, all_imgs, left_imgs, right_imgs = GetFilesFromDir(input)
    if len(all_imgs) == 0:
        print("No images found in {}".format(input))
    else:
        CreateMonoBag(path, all_imgs, ouput)


if __name__ == "__main__":
    if len(sys.argv) == 3:
        CreateBag(sys.argv[1], sys.argv[2])
    else:
        print("Usage: img2bag imagedir bagfilename")
