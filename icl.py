"""Download and convert a set of ICL-NUIM sequences."""
import os.path
import csv
import re
import subprocess


def parseDataset(filename):
    """Process a CSV table with pairs (seq, tar filename)."""
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        datasets = [row for row in reader]
    return datasets


def execute(cmd):
    """Run a command as subprocess."""
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = proc.communicate()


def download(url, path):
    """Download a file from url."""
    execute('wget -nc -P {} {}'.format(path, url))


def unzip(src, path, dst):
    """Extract a file using 7zip."""
    print('extracting {} from {} to {}'.format(path, src, dst))
    execute('7z x -O{} {} {}'.format(dst, src, path))


def untar(src, path, dst):
    """Untar file with a external command."""
    print('extracting {} from {} to {}'.format(path, src, dst))
    execute('tar -C {} -xvf {} --wildcards {}'.format(dst, src, path))


def findWilcardFile(path, card):
    """Find a file matching a glob expression."""
    import fnmatch
    result = []
    for f in os.listdir(path):
        if fnmatch.fnmatch(f, card):
            result.append(f)
    return result


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='a csv with the sequences to download')
    args = parser.parse_args()

    datasets = parseDataset(args.input)
    path = os.path.dirname(args.input)

    for sequence in datasets:
        dst = os.path.join(path, sequence[0])
        if os.path.exists(dst):
            print('path {} already exists; go to next sequence'.format(dst))
            continue

        os.makedirs(dst)  # tar does not create the dir
        download(sequence[1], path)

        # Extract only RGB images and GT
        filename = sequence[1].rsplit('/', 1)[-1]
        gtwc = '*.gt.freiburg'
        fnextract = 'rgb associations.txt {}'.format(gtwc)
        untar(os.path.join(path, filename), fnextract, dst)

        # Fix timestamps and write only RGB file associations
        asso = os.path.join(dst, 'associations.txt')
        rgbo = os.path.join(dst, 'rgb.txt')
        with open(asso, 'r') as assf:
            reader = csv.reader(assf, delimiter=' ')
            with open(rgbo, 'w') as fo:
                writer = csv.writer(fo, delimiter=' ')
                for row in reader:
                    t = float(row[2]) / 30.0  # Run at 30 Hz
                    writer.writerow(['{:10.4f}'.format(t), row[3]])

        # Convert the ground truth specified by the wildcard
        gti = os.path.join(dst, findWilcardFile(dst, gtwc)[0])
        gto = '{}.gt.txt'.format(dst)
        with open(gti, 'r') as gtif:
            reader = csv.reader(gtif, delimiter=' ')
            with open(gto, 'w') as gtof:
                writer = csv.writer(gtof, delimiter=' ')
                for row in reader:
                    row[0] = float(row[0]) / 30.0  # Run at 30 Hz
                    writer.writerow(row)

        # Remove unnecessary associations.txt and original GT
        execute('rm {} {}'.format(asso, gti))
