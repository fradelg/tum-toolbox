"""
Download a KITTI sequence from NAS and convert to TUM format.

It converts associations in times.txt file to a rgb.txt.
"""
import os.path
import subprocess


def execute(cmd):
    """Call an external executable and read stdout and stderr."""
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = proc.communicate()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='the path containing KITTI sequences')
    parser.add_argument('first', type=int, default=0,
                        help='the first sequence to convert')
    parser.add_argument('last', type=int, default=10,
                        help='the last sequence to convert')
    args = parser.parse_args()

    for seq in range(args.first, args.last + 1):
        seq = '{:02d}'.format(seq)
        dst = os.path.join(args.path, seq)

        print('syncing images of sequence {}'.format(seq))
        src = 'root@nas.mobivap.es:/shares/fradelg/bags/kitti/{}/image_0'.format(seq)
        execute('rsync -avh {}/ {}/image_0/'.format(src, dst))

        # Convert timestamp-pose format into TUM
        datafile = os.path.join(dst, 'times.txt')
        rgbfile = os.path.join(dst, 'rgb.txt')
        with open(datafile, 'r') as f:
            times = f.readlines()
            with open(rgbfile, 'w') as fo:
                nimg = 0
                for timestamp in times:
                    ts = float(timestamp.rstrip('\r\n'))
                    fo.write('{:10.6f} image_0/{:06d}.png\n'.format(ts, nimg))
                    nimg += 1

        # execute('rm -rf {}'.format(os.path.join(dst, 'image_1')))
        # execute('rm {}'.format(os.path.join(dst, 'calib.txt')))
        # execute('rm {}'.format(os.path.join(dst, 'times.txt')))
