"""Download, parse and clean the sequences of the TUM RGB-D dataset."""
import os.path
import subprocess
import icl


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='the CSV file with TUM sequences links')
    args = parser.parse_args()

    datasets = icl.parseDataset(args.input)
    path = os.path.dirname(args.input)

    for dataset in datasets:
        dst = os.path.join(path, dataset[0])
        if os.path.exists(dst):
            print('path {} is already there; go to next'.format(dst))
            continue

        os.makedirs(dst)  # tar does not create the dir
        icl.download(dataset[1], path)
        filename = dataset[1].rsplit('/', 1)[-1]
        tarfile = os.path.join(path, filename)
        base, ext = os.path.splitext(filename)
        tardir = os.path.join(dst, base)

        # Files are tarred in a directory named as the file
        icl.untar(tarfile, os.path.join(base, 'rgb'), dst)
        icl.untar(tarfile, os.path.join(base, 'rgb.txt'), dst)

        # Move contents to the upper dir
        icl.execute('mv {}/rgb {}'.format(tardir, dst))
        icl.execute('mv {}/rgb.txt {}'.format(tardir, dst))
        icl.execute('rm -rf {}'.format(tardir))
