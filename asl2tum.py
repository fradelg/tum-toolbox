"""Parse ASL poses and export to TUM RGB-D format."""
import sys
import glob
import csv
import yaml
import numpy as np
import quaternion


class Pose:
    """The pose of a frame (position + orientation) with its corresponding timestamp."""

    def __init__(self, t, p, q):
        """Create a new pose."""
        self.t = t
        self.p = [float(v) for v in p]
        self.q = [float(v) for v in q]

    def __str__(self):
        """Serialize this pose."""
        return "{} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f} {:1.8f}".format(
            self.t, self.p[0], self.p[1], self.p[2], self.q[0], self.q[1], self.q[2], self.q[3])

    @property
    def matrix(self):
        """Return the 4x4 matrix for the homogeneous transformation."""
        m = np.identity(4)
        q = quaternion.quaternion(self.q[3], self.q[0], self.q[1], self.q[2])
        q_m = quaternion.as_rotation_matrix(q)
        for i in range(0, 3):
            for j in range(0, 3):
                m[i, j] = q_m[i, j]
        for i in range(0, 3):
            m[i, 3] = self.p[i]
        return m


def parse_ASL(filename):
    """Retrieve all poses from a data.csv file in the ASL format."""
    poses = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        headers = True
        for row in reader:
            if headers:
                headers = False
                continue
            t = float(row[0]) / 1e9
            p = [row[1], row[2], row[3]]  # x y z
            q = [row[5], row[6], row[7], row[4]]  # x y z w
            pose = Pose(t, p, q)
            poses.append(pose)

    return poses


def parse_TUM(filename):
    """Retrieve all poses from a CVS file in the TUM format."""
    poses = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            if row[0] is '#':
                continue
            t = float(row[0])
            p = [row[1], row[2], row[3]]
            q = [row[4], row[5], row[6], row[7]]
            poses.append(Pose(t, p, q))

    return poses


def transform_to_cam(sensor_fn, poses_b):
    """
    Apply the T_BS transformation of the sensor ref. frame to camera poses.

    :param sensor_fn: the YAML file with the extrinsic calibration in ASL format
    :param poses_b: the list of poses to apply the transformation
    """
    data = None
    with open(sensor_fn, "r") as sfi:
        data = yaml.load(sfi)
    t_bs = np.array(data['T_BS']['data']).reshape(4, 4)
    poses_s = []
    for pose in poses_b:
        mat = np.matmul(pose.matrix, t_bs)
        q = quaternion.from_rotation_matrix(mat[0:3, 0:3])
        pose = Pose(pose.t, mat[:, 3], [q.x, q.y, q.z, q.w])
        poses_s.append(pose)
    return poses_s


def write_TUM(poses, filename):
    """Write the list of poses in the TUM RGB-D format."""
    with open(filename, "w") as text_file:
        for pose in poses:
            text_file.write('{}\n'.format(pose))


def transform_all(src, dst, sensor):
    """
    Apply a reference frame transformation to a TUM RGB-D file.

    :param src: the source filename in ASL format with the list of poses
    :param dst: the filename of the target to save in TUM format
    :param sensor: the name of the sensor file in YAML format
    """
    poses_b = parse_ASL(src)
    poses_s = transform_to_cam(sensor, poses_b)
    write_TUM(poses_s, dst)


if __name__ == "__main__":
    if len(sys.argv) == 4:
        transform_all(sys.argv[2], sys.argv[3], sys.argv[1])
    elif len(sys.argv) == 3:
        gts = glob.glob('{}/*.gt2.txt'.format(sys.argv[2]))
        for gti in gts:
            gto = gti.replace('.gt2.txt', '.gt.txt')
            poses_b = parse_TUM(gti)
            poses_s = transform_to_cam(sys.argv[1], poses_b)
            write_TUM(poses_s, gto)
    else:
        print("Usage: asl2tum <sensor-file> <tum-file-src> <tum-file-dst>")
        print("Usage: asl2tum <sensor-file> <target-dir>")
